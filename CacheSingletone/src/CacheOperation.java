import java.util.List;

public interface CacheOperation {

	void items(String key, List<EnumValue> items);
	void operation();
	
}
