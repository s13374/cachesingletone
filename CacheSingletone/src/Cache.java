import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Cache {

	private static Cache instance;
	private static Object monitor = new Object();
	private Map<String,List<EnumValue>> items = new HashMap<String,List<EnumValue>>();
	
	private Cache() {}

	public static Cache getInstance() {
		if(instance == null) {
			synchronized(monitor) {
				if(instance == null) {
					instance = new Cache();
				}	
			}
		}
		return instance;		
	}
	
	public List<EnumValue> getCacheData(String key) {
		return this.items.get(key);
	}
	
	public void put(String key, List<EnumValue> value) {
		items.put(key, value);
	}
	
	public void clear() {
		this.items.clear();
	}

		
}