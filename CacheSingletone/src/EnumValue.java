public class EnumValue {

	private int id;
	private int enumId;
	private String code;
	private String value;
	private String enumerationName;
	
	
	public void setId(int id) {
		this.id = id;
	}

	public void setEnumId(int enumId) {
		this.enumId = enumId;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public void setEnumerationName(String enumerationName) {
		this.enumerationName = enumerationName;
	}

	public EnumValue(int id, int enumId, String code, String value, String enumerationName) {
		setId(id);
		setEnumId(enumId);
		setCode(code);
		setValue(value);
		setEnumerationName(enumerationName);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ID: " + id + ", Enum ID: " + enumId + ", Code: " + code + ", Value: " + value
				+ ", Enum: " + enumerationName + "]\n");
		return builder.toString();
	}

}